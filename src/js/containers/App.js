import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../actions/index.js';
import Sidebar from '../components/Sidebar.js';
import NightMode from '../components/nightmode.js';
import Template from '../components/template.js';

//const app is the current state of the entire application
const App = ({actions, workExperiences, workResponsibilities, pieChart, educations, languages, certifications, skills, summary, profile, styles, pages}) => (
	<div className="cvEditor" id="cvEditor">
		<div className="col-md-3 col-sm-0 sidebar">
				<Sidebar pages={pages}
					actions={actions} 
					workExperiences={workExperiences} 
					workResponsibilities={workResponsibilities} 
					pieChart={pieChart} 
					educations={educations} 
					languages={languages} 
					certifications={certifications} 
					skills={skills} 
					summary={summary} 
					profile={profile} 
					styles={styles}/>
				<NightMode />
		</div>
		<div className="col-md-9 content" id="content">
			<Template 
			pages={pages}
			actions={actions} 
			workExperiences={workExperiences} 
			workResponsibilities={workResponsibilities} 
			pieChart={pieChart} 
			educations={educations} 
			languages={languages} 
			certifications={certifications} 
			skills={skills} 
			summary={summary} 
			profile={profile} 
			styles={styles} />
		</div>
			<div className="tool" id="tool">
				<div className="closeIt" id="closeIt"><div className="icono-cross" className="stopTooltip"></div></div>
				<p className="tip" id="profileTip">
						Profielschets dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="intrestsTip">
						Intresses dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="certificationTip">
						Certificaten dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="languageTip">
						Talen dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="educationTip">
						Opleidingen dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="workTip">
						Werkervaring dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
				<p className="tip" id="skillsTip">
						Vaardigheden dit is een text waarin je informatie kan zetten over het onderwerp. alle topics zijn uniek en hebben een eigen plekje met text
				</p>
			</div>
	</div>
)

App.propTypes = {
	actions: PropTypes.object.isRequired,
	workExperiences: PropTypes.array.isRequired,
	workResponsibilities: PropTypes.array.isRequired,
	educations: PropTypes.array.isRequired,
	languages: PropTypes.array.isRequired,
	certifications: PropTypes.array.isRequired,
	skills: PropTypes.array.isRequired,
	summary: PropTypes.array.isRequired,
	profile: PropTypes.array.isRequired,
	styles: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
	workExperiences: state.workExperiences,
	workResponsibilities: state.workResponsibilities,
	educations: state.educations,
	languages: state.languages,
	certifications: state.certifications,
	skills: state.skills,
	summary: state.summary,
	profile: state.profile,
	styles: state.styles,
	pages: state.pages
})

const mapDispatchToProps = dispatch => ({
		actions: bindActionCreators(actionCreators, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(App)

