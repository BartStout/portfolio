export const addWorkExperience = (text, workExperienceId) => {
	return {
	type: 'ADD_WORK_EXPERIENCE',
		text,
		id: workExperienceId
	}
}

export const addWorkResponsibility = (workResponsibilityId, parentId) => {
	return {
		type: 'ADD_WORK_RESPONSIBILITY',
		id: workResponsibilityId,
		work_id: parentId
	}
}

export const editWorkResponsibility = (id, text) => {
	return {
		type: "UPDATE_WORK_RESPONSIBILITY",
		id,
		text
	}
}

export const deleteWorkExperience = (id) => {
	return {
		type: 'REMOVE_WORK_EXPERIENCE',
		id
	}
}

export const deleteWorkResponsibility = (id, work_id) => {
	return {
		type: 'REMOVE_WORK_RESPONSIBILITY',
		id,
		work_id
	}
}

export const editWorkRole = (id, text) => {
	return {
		type: 'CHANGE_WORKROLE',
		id,
		text
	}
}

export const editWorkPlace = (id, text) => {
	return {
		type: 'CHANGE_WORKPLACE',
		id,
		text
	}
}

export const editWorkCompany = (id, text) => {
    return {
        type: 'EDIT_WORK_COMPANY',
        id,
        text
    }
}

export const editSliderLabel = (id, text) => {
	return {
		type: 'EDIT_SLIDER_TEXT',
		id,
		text
	}
}

export const editSliderValue = (id, value) => {
    return {
        type: 'EDIT_SLIDER_VALUE',
        id,
        value
    }
}

export const deleteSlider = (id) => {
	return {
		type: 'DELETE_SLIDER',
		id
	}
}

export const addPie = (id) => {
	return {
		type: 'ADD_PIE',
		id
	}
}

export const loadPie = (pie) => {
	return {
		type: "LOAD_PIE",
		pie
	}
}

export const changeEducationName = (id, text) => {
	return {
		type: "CHANGE_EDUCATION_NAME",
		id,
		text
	}
}

export const changeEducationLocation = (id, text) => {
	return {
		type: "CHANGE_EDUCATION_LOCATION",
		id,
		text
	}
}

export const changeEducationInformation = (id, text) => {
	return {
		type: "CHANGE_EDUCATION_INFORMATION",
		id,
		text
	}
}

export const addEducation = (id, name) => {
	return {
		type: "ADD_EDUCATION",
		id,
		name
	}
}

export const deleteEducation = (id) => {
	return {
		type: "DELETE_EDUCATION",
		id
	}
}

export const changeCertificationName = (id, text) => {
	return {
		type: "CHANGE_CERTIFICATION_NAME",
		id,
		text
	}
}

export const changeCertificationLocation = (id, text) => {
	return {
		type: "CHANGE_CERTIFICATION_LOCATION",
		id,
		text
	}
}

export const changeCertificationInformation = (id, text) => {
	return {
		type: "CHANGE_CERTIFICATION_INFORMATION",
		id,
		text
	}
}

export const addCertification = (id, name) => {
	return {
		type: "ADD_CERTIFICATION",
		id
	}
}

export const deleteCertification = (id) => {
	return {
		type: "DELETE_CERTIFICATION",
		id
	}
}

// language

export const createLanguage = (id) => {
	return {
		type: "CREATE_LANGUAGE",
		id
	}
}
export const deleteLanguage = (id) => {
	return {
		type: "DELETE_LANGUAGE",
		id
	}
}

export const editLanguage = (id, text) => {
	return {
		type: "EDIT_LANGUAGE",
		id,
		text
	}
}

export const editLanguageWriting = (id, last) => {
	return {
		type: "EDIT_LANGUAGE_WRITING",
		id,
		last
	}
}

export const editLanguageSpeaking = (id, last) => {
	return {
		type: "EDIT_LANGUAGE_SPEAKING",
		id,
		last
	}
}

// SKILL

export const createSkill = (id) => {
	return {
		type: "CREATE_SKILL",
		id
	}
}

export const deleteSkill = (id) => {
	return {
		type: "DELETE_SKILL",
		id
	}
}

export const editSkill = (id, text) => {
	return {
		type: "EDIT_SKILL",
		id,
		text
	}
}

export const editSkillWriting = (id, last) => {
	return {
		type: "EDIT_SKILL_WRITING",
		id,
		last
	}
}

export const addSummary = () => {
	return {
		type: "CREATE_SUMMARY"
	}
}

export const editSummary = (id, text) => {
	return {
		type: "EDIT_SUMMARY",
		id,
		text
	}
}

export const addProfile = () => {
	return {
		type: "CREATE_PROFILE"
	}
}

export const editProfileTitle = (id, text) => {
	return {
		type: "EDIT_PROFILE_TITLE",
		id,
		text
	}
}

export const editProfileSubTitle = (id, text) => {
	return {
		type: "EDIT_PROFILE_SUBTITLE",
		id,
		text
	}
}

export const editProfileBirth = (id, text) => {
	return {
		type: "EDIT_PROFILE_BIRTH",
		id,
		text
	}
}

export const editProfilePhone = (id, text) => {
	return {
		type: "EDIT_PROFILE_PHONE",
		id,
		text
	}
}

export const editProfileLocation = (id, text) => {
	return {
		type: "EDIT_PROFILE_LOCATION",
		id,
		text
	}
}

export const editProfileEmail = (id, text) => {
	return {
		type: "EDIT_PROFILE_EMAIL",
		id,
		text
	}
}

export const editProfileLinked = (id, text) => {
	return {
		type: "EDIT_PROFILE_LINKED",
		id,
		text
	}
}

export const editFont = (text) => {
	return {
		type: "EDIT_FONT",
		text
	}
}

export const editColor = (text) => {
	return {
		type: "EDIT_COLOR",
		text
	}
}

export const editSkillSliders = (text) => {
	return {
		type: "EDIT_SKILL_SLIDERSTYLE",
		text
	}
}

export const editLanguageSliders = (text) => {
	return {
		type: "EDIT_LANGUAGE_SLIDERSTYLE",
		text
	}
}

export const newTemplate = (state) => {
	return {
		type: "NEW_PAGE",
		state
	}
}

export const newEmptyTemplate = () => {
	return {
		type: "NEW_PAGE"
	}
}


export const changePage = (category, id, page, change) => {
	return {
		type: "CHANGE_PAGE",
		category,
		id,
		page,
		change
	}
}

export const placePage = (id, category, page) => {
	return {
		type: "PLACE_IN_PAGE",
		id,
		category,
		page

	}
}

export const checkPageForDelete = (page) => {
	return {
		type: "CHECK_FOR_REMOVE",
		page
	}
}

export const removePage = (id, category, page) => {
	return {
		type: "REMOVE_FROM_PAGE",
		id,
		category,
		page
	}
}