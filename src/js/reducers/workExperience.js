const workExperiencesReducer = (state = [], action) => {
	switch (action.type) {
		case "CHANGE_WORKPLACE":
			let newCompanyPlace = Object.assign([], state);
			let targetedPlace = newCompanyPlace.findIndex(function(work){
				return work.id === action.id
			})
			newCompanyPlace[targetedPlace].place = action.text;
			return newCompanyPlace;
		case "EDIT_WORK_COMPANY":
			let newCompanyState = Object.assign([], state);
			let targetedCompany = newCompanyState.findIndex(function(work){
				return work.id === action.id
			})
			newCompanyState[targetedCompany].company = action.text;
			return newCompanyState;
		case "CHANGE_WORKROLE":
			let newCompanyRole = Object.assign([], state);
			let targetedRole = newCompanyRole.findIndex(function(work){
				return work.id === action.id
			})
			newCompanyRole[targetedRole].jobTitle = action.text;
			return newCompanyRole;
		case "ADD_WORK_EXPERIENCE": 
			state = [...state, newWorkExperience(undefined, action)];
			return state;
		case "REMOVE_WORK_EXPERIENCE":
			state = [...state.filter(function(e){ return e.id !== action.id })]
			return state;
		case "ADD_WORK_RESPONSIBILITY":
			let id = action.id;
			let parent = action.work_id;
			let newState = Object.assign([], state);
			let targetParent = newState.findIndex(function(workResp) {
				return workResp.id === parent;
			});
			newState[targetParent].responsibilities.push(id);
			return newState;
		case "REMOVE_WORK_RESPONSIBILITY":
			let newS = Object.assign([], state);
			let targetExp = newS.findIndex(function(workExp){
				return workExp.id = action.work_id
			});
			let newResps = newS[targetExp].responsibilities.filter(function(e) {
				return e !== action.id
			});
			newS[targetExp].responsibilities = newResps;
			return newS;
		default:
			return state;
			break;
	}
}

const newWorkExperience = (state={} , action) => {
	return ({
	  id: action.id,
	  company: "",
	  jobTitle: "",
	  place: "",
	  responsibilities: [],
	});
}


export default workExperiencesReducer