const skillsReducer = (state = [], action) => {
	switch (action.type) {
		case "CREATE_SKILL":
			state = [...state, newSkill(undefined, action)];
			return state;
		case "DELETE_SKILL":
			state = [...state.filter(function(e){ return e.id !== action.id })];
			return state;
		case "EDIT_SKILL":
			let newSkillState = Object.assign([], state);
			let targetedSkill = newSkillState.findIndex(function(skill){
				return skill.id === action.id
			})
			newSkillState[targetedSkill].skill = action.text;
			return newSkillState;
		case "EDIT_SKILL_WRITING":
			let newSkillLastState = Object.assign([], state);
			let targetedSkillLastSkill = newSkillLastState.findIndex(function(skill){
				return skill.id === action.id
			})
			newSkillLastState[targetedSkillLastSkill].lastSkill = action.last;
			return newSkillLastState;
		default:
			return state;
			break;
	}
}

const newSkill = (state={} , action) => {
	return ({
	  id: action.id,
	  skill: "",
	  lastSkill: 6
	});
}

export default skillsReducer;