const profileReducer = (state = [], action) => {
	switch (action.type) {
		case "CREATE_PROFILE":
			state = [...state, newProfile(undefined)];
			return state;
		case "EDIT_PROFILE_TITLE":
			let newProfileTitleState = Object.assign([], state);
			let targetedProfileTitle = newProfileTitleState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileTitleState[targetedProfileTitle].title = action.text;
			return newProfileTitleState;
		case "EDIT_PROFILE_SUBTITLE":
			let newProfileSubTitleState = Object.assign([], state);
			let targetedProfileSubTitle = newProfileSubTitleState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileSubTitleState[targetedProfileSubTitle].subtitle = action.text;
			return newProfileSubTitleState;
		case "EDIT_PROFILE_BIRTH":
			let newProfileBirthState = Object.assign([], state);
			let targetedProfileBirth = newProfileBirthState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileBirthState[targetedProfileBirth].birth = action.text;
			return newProfileBirthState;
		case "EDIT_PROFILE_PHONE":
			let newProfilePhoneState = Object.assign([], state);
			let targetedProfilePhone = newProfilePhoneState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfilePhoneState[targetedProfilePhone].phone = action.text;
			return newProfilePhoneState;
		case "EDIT_PROFILE_EMAIL":
			let newProfileEmailState = Object.assign([], state);
			let targetedProfileEmail = newProfileEmailState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileEmailState[targetedProfileEmail].email = action.text;
			return newProfileEmailState;
		case "EDIT_PROFILE_LOCATION":
			let newProfileLocationState = Object.assign([], state);
			let targetedProfileLocation = newProfileLocationState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileLocationState[targetedProfileLocation].location = action.text;
			return newProfileLocationState;
		case "EDIT_PROFILE_LINKED":
			let newProfileLinkedState = Object.assign([], state);
			let targetedProfileLinked = newProfileLinkedState.findIndex(function(profile){
				return profile.id === action.id
			})
			newProfileLinkedState[targetedProfileLinked].linked = action.text;
			return newProfileLinkedState;
		default:
			return state;
			break;
	}
}

const newProfile = (state={} , action) => {
	return ({
	  id: 1,
	  title: "",
	  subtitle: "",
	  birth: "",
	  phone: "",
	  email: "",
	  location: "",
	  linked: ""
	});
}

export default profileReducer;