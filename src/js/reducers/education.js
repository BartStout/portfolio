const educationReducer = (state = [], action) => {
	switch (action.type) {
		case "ADD_EDUCATION":
			state = [...state, newEducation(undefined, action)];
			
			return state;
		case "CHANGE_EDUCATION_NAME":
			let newEductionName = Object.assign([], state);
			let targetedEducationName = newEductionName.findIndex(function(education){
				return education.id === action.id
			})
			newEductionName[targetedEducationName].name = action.text;
			return newEductionName;
		case "CHANGE_EDUCATION_LOCATION":
			let newEductionLocation = Object.assign([], state);
			let targetedEducationLocation = newEductionLocation.findIndex(function(education){
				return education.id === action.id
			})
			newEductionLocation[targetedEducationLocation].location = action.text;
			return newEductionLocation;
		case "CHANGE_EDUCATION_INFORMATION":
			let newEductionInformation = Object.assign([], state);
			let targetedEducationInformation = newEductionInformation.findIndex(function(education){
				return education.id === action.id
			})
			newEductionInformation[targetedEducationInformation].info = action.text;
			return newEductionInformation;
		case "DELETE_EDUCATION":
			state = [...state.filter(function(e){ return e.id !== action.id })];
			return state;
		default:
			return state;
			break;
	}
}

const newEducation = (state={} , action) => {
	return ({
	  id: action.id,
	  name: "",
	  location: "",
	  info: ""
	});
}


export default educationReducer