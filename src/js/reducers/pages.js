const pageReducer = (state = {}, action) => {
	switch(action.type) {
		case "NEW_PAGE":
			//action may have id
			// return new page
			if(action.id == undefined) {
				action.id = 1;
				state = [...state, newPage(undefined, action)];
				state[0].items.profile = [];
				state[0].items.summary = [];
				state[0].items.workExperience = [];
				// state[0].items.skills = [];
				state[0].items.languages = [];
				state[0].items.educations = [];
				state[0].items.certifications = [];
				state[0].items.pieChart = [];
				state[0].items.profile.push(1);
				state[0].items.summary.push(1);
				state[0].items.workExperience.push(1);
				// state[0].items.skills.push(1);
				state[0].items.educations.push(1);
				state[0].items.languages.push(1);
				state[0].items.pieChart.push(1);
				state[0].items.certifications.push(2);
				return state;
			} else {
				action.id = action.id + 1;
				state = [...state, newPage(undefined, action)];
				return state;
			}
		case "CHECK_FOR_REMOVE":
			// action containes page id
			//in the state[page] check if there are items left in the page
			let decider = 0;
			for(let k in state[action.page - 1].items) {
				decider = decider + state[action.page - 1].items[k].length;
			}
			if(decider == 0) {
				//still buggy, need extra intell;
				let pages = Object.assign([], state);
				pages = pages.filter(function(page) {
					return page.page !== action.page;
				});
				return pages;
			}
		case "CHANGE_PAGE":
			if(action.change == 'UP') {
				let cat = action.category;
				let componentId = action.id;
				let thisPage = action.page;
				let newState = Object.assign([], state);
				let oldPage = newState[thisPage - 1];
				oldPage.items[cat] = oldPage.items[cat].filter(function(e){ return e !== action.id });
				if(newState[thisPage] == undefined) {
					let newId = thisPage + 1;
					let newPageForCat = {'page': newId, 'items': {}};
					newState.push(newPageForCat);
					newState[thisPage].items[cat] = [];
				} else {
					if(newState[thisPage].items[cat] == undefined) {
						newState[thisPage].items[cat] = [];
					}
				}
				let newPage = newState[thisPage];
				newPage.items[cat].push(action.id);
				return newState;
			} else if(action.change == 'DOWN') {
				let cat = action.category;
				let componentId = action.id;
				let thisPage = action.page;
				let newState = Object.assign([], state);
				let oldPage = newState[thisPage - 1];
				oldPage.items[cat] = oldPage.items[cat].filter(function(e){ return e !== action.id });
				if(newState[thisPage - 2].items[cat] == undefined) {
					newState[thisPage - 2].items[cat] = [];
				}
				let newPage = newState[thisPage - 2];
				newPage.items[cat].push(action.id);
				return newState;
			} else {
				return state;
			}
		case "PLACE_IN_PAGE":
			//action needs action.target and action.page and action.element and action.target
			let cat = action.category;
			let componentId = action.id;
			let thisPage = action.page;
			let newState = Object.assign([], state);
			let targetedPage = (thisPage - 1);
			//check if cat already exists
			if(newState[targetedPage].items[cat] == undefined) {
				newState[targetedPage].items[cat] = [];
			} else {
				//cat already exists, no worries
			}
			newState[targetedPage].items[cat].push(componentId);
			return newState;
		case "REMOVE_FROM_PAGE":
			newState = Object.assign([], state);
			newState[action.page - 1].items[action.category] = newState[action.page - 1].items[action.category].filter(function(e){
				return e !== action.id
			});
			return newState;
		default:
			return state;
	}
}

const newPage = (state={} , action) => {
	return ({
	  page: action.id,
	  items: {
	  	
	  }
	});
}

export default pageReducer;