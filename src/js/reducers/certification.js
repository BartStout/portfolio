const certificationReducer = (state = [], action) => {
	switch (action.type) {
		case "ADD_CERTIFICATION":
			state = [...state, newCertification(undefined, action)];
			return state;
		case "CHANGE_CERTIFICATION_NAME":
			let newCertificationName = Object.assign([], state);
			let targetedCertificationName = newCertificationName.findIndex(function(certification){
				return certification.id === action.id
			})
			newCertificationName[targetedCertificationName].name = action.text;
			return newCertificationName;
		case "CHANGE_CERTIFICATION_INFORMATION":
			let newCertificationInformation = Object.assign([], state);
			let targetedCertificationInformation = newCertificationInformation.findIndex(function(certification){
				return certification.id === action.id
			})
			newCertificationInformation[targetedCertificationInformation].instituut = action.text;
			return newCertificationInformation;
		case "DELETE_CERTIFICATION":
			state = [...state.filter(function(e){ return e.id !== action.id })];
			return state;
		default:
			return state;
			break;
	}
}

const newCertification = (state={} , action) => {
	return ({
	  id: action.id,
	  name: "",	
	  instituut: ""
	});
}


export default certificationReducer