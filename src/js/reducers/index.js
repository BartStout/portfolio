import { combineReducers } from 'redux';
import workExperiencesReducer from './workExperience.js';
import workResponsibilityReducer from './workResponsibility.js';
import pieChartReducer from './pieChart.js';
import educationReducer from './education.js';
import languageReducer from './languages.js';
import certificationReducer from './certification.js';
import skillsReducer from './skill.js';
import summaryReducer from './summary.js';
import profileReducer from './profile.js';
import styleReducer from './styles.js';
import pageReducer from './pages.js';

const reducers = combineReducers({
	workExperiences: workExperiencesReducer,
	workResponsibilities: workResponsibilityReducer,
	pieChart: pieChartReducer,
	educations: educationReducer,
	languages: languageReducer,
	certifications: certificationReducer,
	skills: skillsReducer,
	summary: summaryReducer,
	profile: profileReducer,
	styles: styleReducer,
	pages: pageReducer
})

export default reducers
