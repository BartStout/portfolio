const workResponsibilityReducer = (state = [], action) => {
  switch (action.type) {
	case "ADD_WORK_RESPONSIBILITY":
		state = [...state, newWorkResponsibility(undefined, action)];
		return state;
	case "REMOVE_WORK_RESPONSIBILITY":
		state = [...state.filter(function(e){ return e.id !== action.id })];
		return state;
	case "UPDATE_WORK_RESPONSIBILITY": 
		let newResponsibilityState = Object.assign([], state);
		let targetedResponsibility = newResponsibilityState.findIndex(function(responsibility){
			return responsibility.id === action.id
		})
		newResponsibilityState[targetedResponsibility].task = action.text;
		return newResponsibilityState;
	case "REMOVE_WORK_EXPERIENCE":
		state = [...state.filter(function(e){ return e.work_id !== action.id })];
		return state;
	default:
		return state;
	break;
  }
}

const newWorkResponsibility = (state={} , action) => {
  return {
	id: action.id,
	work_id: action.work_id,
	task: ""
  }
}	

export default workResponsibilityReducer