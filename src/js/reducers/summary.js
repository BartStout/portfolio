const summaryReducer = (state = [], action) => {
	switch (action.type) {
		case "CREATE_SUMMARY":
			state = [...state, newSummary(undefined)];
			return state;
		case "EDIT_SUMMARY":
			let newSummaryState = Object.assign([], state);
			let targetedSummary = newSummaryState.findIndex(function(summary){
				return summary.id === action.id
			})
			newSummaryState[targetedSummary].summary = action.text;
			return newSummaryState;
		default:
			return state;
			break;
	}
}

const newSummary = (state={} , action) => {
	return ({
	  id: 1,
	  summary: ""
	});
}

export default summaryReducer;