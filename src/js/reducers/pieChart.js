const pieChartReducer = (state = {}, action) => {
  switch (action.type) {
    case "EDIT_SLIDER_TEXT":
      let newLabelState = Object.assign({}, state);
      let newLabelDataState = Object.assign([], newLabelState.data.labels);
      let newLabelStateTarget = newLabelState.data.sliceId.findIndex(function(sliceId){
        return sliceId === action.id;
      });
      newLabelState.data.labels[newLabelStateTarget] = action.text
      return newLabelState;
    case "EDIT_SLIDER_VALUE":
      let newValueState = Object.assign({}, state);
      let newDataState = Object.assign([], newValueState.data.datasets[0].data);
      let newValueStateTarget = newValueState.data.sliceId.findIndex(function(sliceId){
        return sliceId === action.id;
      });
      newValueState.data.datasets[0].data[newValueStateTarget] = action.value
      return newValueState;
    case "ADD_PIE":
        let newS = Object.assign({}, state);
        let newSId = Object.assign([], newS.data.sliceId);
        let newSLabels = Object.assign([], newS.data.labels);
        let newSValues = Object.assign([], newS.data.datasets[0].data) 

        newSId.push(action.id);
        newSLabels.push('');
        newSValues.push(50);

        newS.data.labels = newSLabels;
        newS.data.datasets[0].data = newSValues;
        newS.data.sliceId = newSId;
        return newS;
    case "DELETE_SLIDER":
      let newState = Object.assign({}, state);
      let newIdState = Object.assign([], newState.data.sliceId);
      let newLabelsState = Object.assign([], newState.data.labels);
      let newValuesState = Object.assign([], newState.data.datasets[0].data) 

      let newStateTarget = newState.data.sliceId.findIndex(function(sliceId){
        return sliceId === action.id;
      });

      newIdState = newIdState.filter(function(e, index) {
        return index !== newStateTarget
      });
      newLabelsState = newLabelsState.filter(function(e, index) {
        return index !== newStateTarget
      });
      newValuesState = newValuesState.filter(function(e, index) {
        return index !== newStateTarget
      });

      newState.data.sliceId = newIdState;
      newState.data.labels = newLabelsState;
      newState.data.datasets[0].data = newValuesState;
      return newState;
    case "LOAD_PIE":
      return state;
    default:
      return pieChart;
      break;
  }
}


const pieChart = {
  	//this.props.pieChart.data
  	data: {
  		//this.props.pieChart.data.labels
      sliceId: [
      ],
  		labels: [
  		],
  		//this.props.pieChart.data.datasets
  		datasets: [{
  			//this.props.pieChart.data.datasets.data
  			data: [
        ],
  			//this.props.pieChart.data.datasets.backgroundColor
  			backgroundColor: [
  				'lightseagreen',
          'pink',
          'green',
          'blue',
          'orange',
          'purple',
          'black',
          'red',
          'pink',
          'green',
          'blue',
          'orange',
          'purple',
          'black'
  			],
  		}]
  	}
  };

export default pieChartReducer;