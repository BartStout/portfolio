const styleReducer = (state = {}, action) => {
	switch(action.type) {
		case "EDIT_COLOR":
			let newColorState = Object.assign({}, state);
			newColorState.color = action.text;
			return newColorState;
		case "EDIT_FONT":
			let newFontState = Object.assign({}, state);
			newFontState.font = action.text;
			return newFontState;
		case "EDIT_LANGUAGE_SLIDERSTYLE":
			let newLanguageSliderStyle = Object.assign({}, state);
			newLanguageSliderStyle.sliderStyleLanguage = action.text;
			return newLanguageSliderStyle;
		case "EDIT_SKILL_SLIDERSTYLE":
			let newSkillSliderStyle = Object.assign({}, state);
			newSkillSliderStyle.sliderStyleSkills = action.text;
			return newSkillSliderStyle;
		default: 
			if(state.sliderStyleSkills == undefined) {
				return styles;
			} else {
				return state;
			}
	}
}

const styles = {
	color: 'blue',
	font: 'Lato',
	sliderStyleSkills: 'dot',
	sliderStyleLanguage: 'bar'
}

export default styleReducer;