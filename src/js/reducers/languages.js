const languageReducer = (state = [], action) => {
	switch (action.type) {
		case "CREATE_LANGUAGE":
			state = [...state, newLanguage(undefined, action)];
			return state;
		case "DELETE_LANGUAGE":
			state = [...state.filter(function(e){ return e.id !== action.id })];
			return state;
		case "EDIT_LANGUAGE":
			let newLanguageState = Object.assign([], state);
			let targetedLanguage = newLanguageState.findIndex(function(language){
				return language.id === action.id
			})
			newLanguageState[targetedLanguage].language = action.text;
			return newLanguageState;
		case "EDIT_LANGUAGE_WRITING":
			let newLanguageWritingState = Object.assign([], state);
			let targetedLanguageWritingSkill = newLanguageWritingState.findIndex(function(language){
				return language.id === action.id
			})
			newLanguageWritingState[targetedLanguageWritingSkill].lastWrite = action.last;
			return newLanguageWritingState;
		case "EDIT_LANGUAGE_SPEAKING":
			let newLanguageSpeakingState = Object.assign([], state);
			let targetedLanguageSpeakingSkill = newLanguageSpeakingState.findIndex(function(language){
				return language.id === action.id
			})
			newLanguageSpeakingState[targetedLanguageSpeakingSkill].lastSpeak = action.last;
			return newLanguageSpeakingState;
		default:
			return state;
			break;
	}
}

const newLanguage = (state={} , action) => {
	return ({
	  id: action.id,
	  language: "",
	  lastSpeak: 2,
	  lastWrite: 2
	});
}

export default languageReducer;