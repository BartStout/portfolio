import React, { Component } from 'react';

export default class NightMode extends Component {
	constructor() {
		super();
		this.toggleNightMode = this.toggleNightMode.bind(this);
		this.setCheckbox = this.setCheckbox.bind(this);
		this.state = {
			nightmode: false
		}
	}

	componentDidMount(){
		let time = new Date();
		let timeCheck = time.getHours();
		if(timeCheck >= 21) {
			this.state.nightmode = true;
		} else {
			this.state.nightmode = false;
		}
		this.setCheckbox();
	}
// #29d598 #55f0b9
	setCheckbox() {
		let switchFix = document.getElementById('switch');
		let buttonFix = document.getElementById('downloadCv');
		let body = document.body;
		let checkbox = document.getElementById('nightmode');
		if(this.state.nightmode == true) {
			body.style.backgroundColor = '#5d5d5d';
			checkbox.checked = true;
			switchFix.style.background = '#29d598';
			buttonFix.style.backgroundColor = '#5d5d5d';
		} else {
			body.style.backgroundColor = '#29d598';
			checkbox.checked = false;
			switchFix.style.background = '#5d5d5d';
			buttonFix.style.backgroundColor = '#29d598';
		}
	}

	toggleNightMode() {
		if (this.state.nightmode == true) {
			this.state.nightmode = false;
		} else {
			this.state.nightmode = true;
		}
		let switchFix = document.getElementById('switch');
		let buttonFix = document.getElementById('downloadCv');
		let body = document.body;
		if (this.state.nightmode == true) {
			switchFix.style.background = '#29d598';
			body.style.backgroundColor = '#343434';
			buttonFix.style.backgroundColor = '#5d5d5d';
		} else {
			switchFix.style.background = '#5d5d5d';
			body.style.backgroundColor = '#29d598';
			buttonFix.style.backgroundColor = '#55f0b9';
		}

		this.forceUpdate();
	}

	render() {
		return(
			<div className="nightcontainer">
				<label id="switch" class="switch"><input id="nightmode" type="checkbox" onClick={this.toggleNightMode}/>
					<div class="iconfix"></div>
				</label>
			</div>
		)
	}
}