import React, { Component, PropTypes } from 'react'
import {Pie, Chart, defaults} from 'react-chartjs-2';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../actions/index.js';
import PieOptions from './pieOptions.js';
Chart.pluginService.register({ 
	beforeRender: function(chart) {
		if (chart.config.options.showAllTooltips) { 
			chart.pluginTooltips = []; 
			chart.config.data.datasets.forEach(function(dataset, i) { 
				chart.getDatasetMeta(i).data.forEach(function(sector, j) { 
					chart.pluginTooltips.push(new Chart.Tooltip({ 
						_chart: chart.chart,
						_chartInstance: chart, 
						_data: chart.data, 
						_options: chart.options.tooltips, 
						_active: [sector] 
					}, chart)); 
				}); 
			}); 
			chart.options.tooltips.enabled = false; 
		}

	}, afterDraw: function(chart, easing) { 
		if (chart.config.options.showAllTooltips) { 
			if (!chart.allTooltipsOnce) { 
				if (easing !== 1) return; 
					chart.allTooltipsOnce = true; 
			} chart.options.tooltips.enabled = true; 
			Chart.helpers.each(chart.pluginTooltips, function(tooltip) { 
				tooltip.initialize(); 
				tooltip.update();  
				tooltip.pivot(); 
				tooltip.transition(easing).draw(); 
			}); 
			chart.options.tooltips.enabled = false; 
		} 
	} 
});

let pieOptions = {
	responsive: true,
	showTooltips: false,
	tooltipEvents: [],
	animation : false,
	showAllTooltips: false,
	legend: {
		display: true,
		position: 'bottom'
	},
	tooltips: {
		enabled: false
	}
}

class Piechart extends React.Component {
	constructor() {
		super();
		this.state = {
			pieId: 0,
			ref: ''
		}
	}

	componentWillMount(){
		let init;
		let check = this.props.pieChart.data.sliceId[this.props.pieChart.data.sliceId.length - 1];
		if (check == undefined) {
			init = 0;
			this.addP(this.props);
		} else {
			init = check;
		}
		this.state.pieId = init;
	}

	addP(todo) {
		this.state.pieId++;
		let pieId = this.state.pieId;
		todo.addPie(pieId);
	}

		render() {
			const page = this.props.page;
			const styles = this.props.styles;
			this.state.ref = "pieChart_1_" + page.page;
			let classList = 'headline ' + styles.font;
			if(page.items.pieChart.includes(1)) {
				return (
					<div id="pieRef">
						<h3 className={classList}>Interesses</h3>
						<PieOptions data={this.props} styles={styles}/>
						<div className="dividers">
								<div className="divider fat"></div>
								<div className="divider thin"></div>
						</div>
						<div className="pieChart">  
							<Pie ref={"pie"} id="pie" data={this.props.pieChart.data} redraw={true} options={pieOptions} animation={false} />
						</div>
					</div>
				)
			} else {
				return;
			}
	}
}

function mapStateToProps(state) {
	return {
		pieChart: state.pieChart
	};
}

function matchDispatchToProps(dispatch){
	return bindActionCreators(actionCreators, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Piechart);