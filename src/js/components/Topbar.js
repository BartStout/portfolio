import React from 'react';

class Topbar extends React.Component {
	render() {
		let classFont = this.props.styles.font;
		let classList = 'col-sm-12 topBar ' + this.props.styles.color;
		return(
			<div className={classList}>
				<p className="right">
					<span className={classFont}>Ontwikkeld door: </span><a href="http://www.BrandNewGuys.co"><img src="../src/assets/images/bng-logo-base-white.svg" alt="BrandNewGuys.co" className="bngLogo"/></a>
				</p>
			</div>
		)
	}
}

export default Topbar;