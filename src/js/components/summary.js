	import React, { Component, PropTypes } from 'react';
	import TextArea from './TextArea.js';

	export default class Summary extends Component {
		constructor() {
			super();
			this.state = {
				objectCount: 0
			}
		}

		componentWillMount(){
			let check = this.props.summary.length;
			if(check == 0){
				this.state.objectCount++;
				this.props.actions.addSummary();
			} else {
				this.state.objectCount++;
			}
		}


		renderSummary(props){
			const styles = this.props.styles;
			const action = props.actions 
			return props.summary.map(function(summary){
				return <TextArea styles={styles} key={summary.id} placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id eius officia facilis consectetur commodi, aspernatur cumque consequatur architecto! Fugit provident iusto maiores, explicabo quisquam obcaecati atque officia magni suscipit odio aperiam harum autem neque maxime voluptatibus quis laborum quidem velit dicta, nam a sint, error. Deserunt dolor eos est temporibus quia, blanditiis molestias, vero maxime quaerat magnam maiores veniam tempore." text={summary.summary} onSave={(text) => action.editSummary(1, text)}></TextArea>
			})
		}

		render(){
			const styles = this.props.styles;
			let classList = 'headline Profile profile heading-font col-sm-12 ' + styles.font;
			return(
				<div className="profileSummary">
					<h3 className={classList}>Profielschets</h3>
                          <div className="dividers col-sm-12">
                            <div className="divider fat dividerfix" />
                            <div className="divider thin dividerfix" />
                          </div>
					{this.renderSummary(this.props)}
				</div>
			)
		}
	}