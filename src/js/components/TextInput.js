import React, { Component, PropTypes } from 'react'

export default class TextInput extends React.Component {
  static PropTypes = {
    onSave: PropTypes.func.isRequired,
    text: PropTypes.string,
    placeholder: PropTypes.string,
    editing: PropTypes.bool
  }
  
  state = {
    text: this.props.text || ''
  }

  handleChange = e => {
    this.setState({ text: e.target.value })
  }

  handleBlur = e => {
    this.props.onSave(e.target.value)
  }

  render(){
    const styles = this.props.styles;
    let classList = 'TextInput ' + styles.font;
    return(
      <input type="text"
        className={classList}
        placeholder={this.props.placeholder}
        value={this.state.text}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleSubmit}
      />
    )
  }
}