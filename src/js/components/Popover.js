	import React from 'react';

class PopOver extends React.Component {

	constructor(){
	    super();
	    this.toggle = this.toggle.bind(this);
	    this.toggleActive = this.toggleActive.bind(this);
	    this.onItemClick = this.onItemClick.bind(this);
	    this.state = {
	        hide: true
	    };
	}
	toggle(){
	    this.setState({hide: !this.state.hide});
		let skillOptions = document.getElementById('hiddenSkillsOptions');
		skillOptions.style.display = "inline-block";
		let languageOptions = document.getElementById('hiddenLanguageOptions');
		languageOptions.style.display = "none";
	}
	onItemClick(event) {
		this.props.actions.editSkillSliders(event.target.id);
	    const {id} = event.target;
	    if (id == "dot") {
	        var list = document.querySelectorAll(".skillRating.slider");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('bar');
	            list[i].classList.remove('slider');
	            list[i].classList.add("dot");
	        }
	        var list = document.querySelectorAll(".skillRating.dot");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('bar');
	            list[i].classList.remove('slider');
	            list[i].classList.add("dot");
	        }
	        var list = document.querySelectorAll(".skillRating.bar");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('bar');
	            list[i].classList.remove('slider');
	            list[i].classList.add("dot");
	        }
	    }
	    if (id == "bar") {
	        var list = document.querySelectorAll(".skillRating.slider");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('slider');
	            list[i].classList.add("bar");
	        }
	        var list = document.querySelectorAll(".skillRating.dot");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('slider');
	            list[i].classList.add("bar");
	        }
	        var list = document.querySelectorAll(".skillRating.bar");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('slider');
	            list[i].classList.add("bar");
	        }
	    }
	    if (id == "slider") {
	        var list = document.querySelectorAll(".skillRating.slider");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('bar');
	            list[i].classList.add("slider");
	        }
	        var list = document.querySelectorAll(".skillRating.dot");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('bar');
	            list[i].classList.add("slider");
	        }
	        var list = document.querySelectorAll(".skillRating.bar");
	        for (var i = 0; i < list.length; i++) {
	            list[i].classList.remove('dot');
	            list[i].classList.remove('bar');
	            list[i].classList.add("slider");
	        }
	    }
	}
	toggleActive(event) {
		var list = document.querySelectorAll(".sliderOptions");
		for (var i = 0; i < list.length; i++) {
			list[i].classList.remove('activated');
		};
		event.target.classList.add('activated');
	}

	render() {
		const styles = this.props.styles;
	    return(
	        <div className="rightCog fa fa-cog fa-2x" onClick={this.toggle}>
	            <div className="popover" id="target" className={'hide-' + this.state.hide}>
	                <div className="hiddenPopOver"id="hiddenSkillsOptions">
	                <div className="hiddenSettings">
	                <p>Slider:</p>
	                <div className="fa fa-circle sliderOptions activated" id="slider"    onClick={(e) => { this.onItemClick(e); this.toggleActive(e);}}></div>
	                <div className="fa fa-circle sliderOptions" id="dot"       onClick={(e) => { this.onItemClick(e); this.toggleActive(e);}}></div>
	                <div className="fa fa-circle sliderOptions" id="bar"       onClick={(e) => { this.onItemClick(e); this.toggleActive(e);}}></div>
	                </div>
	                </div>
	            </div>
	        </div>
	    )
	}
}

export default PopOver;