import React, { Component, PropTypes } from 'react'
import TextInput from '../components/TextInput.js';
import TextArea from '../components/TextArea.js';
import WorkResponsibility from '../components/WorkResponsibilities.js';

export default class Work extends React.Component {
	static propTypes = {
		workExperienceId: React.PropTypes.number
	}

	constructor(){
		super();
		this.addNav = this.addNav.bind(this);
		this.state = {
			editing: false,
			workExperienceId: 0,
			workResponsibilityId: 0,
			objectCount: 0
		}
	}

	
	componentWillMount(){
		let init;
		let check = this.props.workExperiences[this.props.workExperiences.length - 1];
		if (check == undefined) {
			init = 0;
		} else {
			this.state.objectCount = this.props.workExperiences.length;
			init = check.id;
		}
		let initResp;
		let checkResp = this.props.workResponsibilities[this.props.workResponsibilities.length - 1];
		if(checkResp == undefined) {
			initResp = 0;
		} else {
			initResp = checkResp.id
		}

		this.setState({
			workExperienceId: init,
			workResponsibilityId: initResp 
		});
		if(this.state.objectCount == 0){
			this.createWork();
		}
	}

	createWork() {
		this.state.workExperienceId++;
		this.state.objectCount++;
		let currentId = this.props.workExperiences[this.props.workExperiences.length - 1].id;
		currentId = currentId + 1;
		this.props.actions.addWorkExperience("", currentId);
		this.props.actions.placePage(currentId, "workExperience", this.props.page.page);
	}

	deleteWork(id) {
		this.state.objectCount--;
		this.props.actions.deleteWorkExperience(id);
		if(this.state.objectCount == 0){
			this.createWork();
		}
		this.props.actions.removePage(id, "workExperience", this.props.page.page);
	}

	createResponsibility(work_id) {
		this.state.workResponsibilityId++
		let currentId = this.state.workResponsibilityId;
		this.props.actions.addWorkResponsibility(currentId, work_id);
	}

	// toggle nav-is-visible

	addNav(e) {
		var d = e.target.parentNode;
		d.classList.toggle('nav-is-visible');
	}

	renderWorkExperience(propData){
		const items = this.props.page.items;
		const page = this.props.page.page;
		const styles = this.props.styles;
		return propData.workExperiences.map((workExperience) => {
			const { editWorkCompany, editWorkRole, editWorkPlace, deleteWorkExperience } = propData.actions;
			let element;
			element = (
				<ul>
					<span className="customBullet">
						<div className="fa fa-circle-o" aria-hidden="true"/>
					</span>
						<TextInput 
							styles={styles}
							onSave={(text) => editWorkRole(workExperience.id, text)} 
							placeholder="Functienaam" 
							text={workExperience.jobTitle}>
						</TextInput>
					<TextInput 
						styles={styles}
						onSave={(text) => editWorkCompany(workExperience.id, text)} 
						placeholder="Werkgever" 
						text={workExperience.company}>
					</TextInput>
					<br/>
					<TextInput 
						styles={styles}
						onSave={(text) => editWorkPlace(workExperience.id, text)} 
						placeholder="Plaats" 
						text={workExperience.place}>
					</TextInput><br/>
				</ul>
			)
			if(items.workExperience.includes(workExperience.id)) {
				return(
					<div className="work" key={workExperience.id} ref={'workExperience_' + workExperience.id + "_" + page}>
						{element}
						<ul> 
							{this.renderWorkResponsibilities(this.props, workExperience)}
						</ul>
						<nav className="cd-stretchy-nav edit-content">
							<a className="cd-nav-trigger" onClick={this.addNav} href="#0">
								<span aria-hidden="true"></span>
							</a>

							<ul>
								<li><a href="#0" onClick={(e) => this.createWork()}></a></li>
								<li><a href="#0" onClick={(e) => this.deleteWork(workExperience.id)}></a></li>
							</ul>

						<span aria-hidden="true" className="stretchy-nav-bg"></span>
						</nav>
					</div>
				) 
			} else {
				return;
			}
		});
	}

	renderWorkResponsibilities(propData, we_data){
		const styles = this.props.styles;
		if(we_data.responsibilities.length == 0){
			this.createResponsibility(we_data.id);
		}
		let wr_data = propData.workResponsibilities;
		return wr_data.map((workResponsibility) => {
				if (workResponsibility.work_id == we_data.id) {
					return <WorkResponsibility styles={styles} key={workResponsibility.id} data={workResponsibility} propData={propData} parentExperience={we_data.id} />
				}
		});
	}

	render() {
		const styles = this.props.styles;
		let classList = 'heading-font headline workExperience ' + styles.font;
		const { workExperiences, actions } = this.props
		return (
			<div className="noBreak">
				<h3 className={classList}> Werkervaring</h3>
				<div className="bordercontainer">
					<div className="dividers">
						<div className="divider fat"></div>
						<div className="divider thin"></div>
					</div>
				</div>
				<div id="workItems">
				{this.renderWorkExperience(this.props)}
				</div>
			</div>
		);
	}
}

