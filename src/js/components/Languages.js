import React, { Component, PropTypes } from 'react';
import TextInput from './TextInput.js';
import PopOverLanguages from './PopOverLanguages.js';

export default class Languages extends Component {
	constructor() {
		super();
		this.setLastSpeak = this.setLastSpeak.bind(this);
		this.setLastWrite = this.setLastWrite.bind(this);
		this.checkLanguage = this.checkLanguage.bind(this);
		this.setOpacity = this.setOpacity.bind(this);
		this.state = {
			languageId: 0,
			objectCount: 0
		}
	}

	componentWillMount(){
		let init;
		let check = this.props.languages[this.props.languages.length - 1];
		let newObjectCount = this.props.languages.length;
		if (check == undefined) {
			init = 0;
		} else {
			init = check.id
		}
		this.state.languageId = init;
		this.state.objectCount = newObjectCount;
		if(this.state.objectCount == 0){
			this.createLanguage();
		}
	}

	componentDidMount(){
		function getNextSiblings(el) {
				let siblings = [];
				while (el= el.nextSibling) { siblings.push(el); }
				return siblings;
		}
		let last = document.querySelectorAll('.last');
		for (let i = 0; i < last.length; i++) {
			let getNextSibs = getNextSiblings(last[i]);
			for (let j = 0; j < getNextSibs.length; j++) {
				getNextSibs[j].style.opacity = "0.2"
			};
		};
	}

	setOpacity(el) {
		function getNextSiblings(el) {
				let siblings = [];
				while (el= el.nextSibling) { siblings.push(el); }
				return siblings;
		}
		function getPreviousSiblings(el) {
			let siblings = [];
			while (el = el.previousSibling) { siblings.push(el); }
			return siblings;
		}
		let nextSibs = getNextSiblings(el);
		let prevSibs = getPreviousSiblings(el);
		for (var i = 0; i < nextSibs.length; i++) {
			nextSibs[i].style.opacity = "0.2";
		}; 
		for (var i = 0; i < prevSibs.length; i++) {
			prevSibs[i].style.opacity = "1";
		};
		el.style.opacity = "1";
	}

	checkLanguage(e, langId) {
		if(e.keyCode == 8){
			if(e.target.value == ""){
				this.deleteLanguage(langId);
			} else {
				//just a backspace
			}
		} else if(e.keyCode == 13) {
			this.createLanguage();
		} else {
			//accept keyinput
		}
	}

	createLanguage() {
		this.state.objectCount++;
		this.state.languageId++;
		let id = this.props.languages[this.props.languages.length - 1].id;
		id = id + 1;
		this.props.actions.createLanguage(id);
		this.props.actions.placePage(id, "languages", this.props.page.page);
	}

	deleteLanguage(id) {
		this.state.objectCount--;
		this.props.actions.deleteLanguage(id);
		if(this.state.objectCount == 0) {
			this.props.actions.createLanguage(1);
			this.state.objectCount++;
		}
		this.props.actions.removePage(id, "languages", this.props.page.page);
	}

	setLastWrite(id, e) {
		let list = e.target.parentNode.children;
		let target = Array.prototype.indexOf.call(list, e.target);
		this.props.actions.editLanguageWriting(id, target);
		this.forceUpdate();
		this.setOpacity(e.target);
	}

	setLastSpeak(id, e) {
		let list = e.target.parentNode.children;
		let target = Array.prototype.indexOf.call(list, e.target);
		this.props.actions.editLanguageSpeaking(id, target);
		this.forceUpdate();
		this.setOpacity(e.target);
	}

	renderLanguages(props){
		const page = this.props.page;
		const styles = this.props.styles;
		let sliderStyle = 'skillSeperator ' + styles.sliderStyleLanguage;
		const checkLanguage = this.checkLanguage;
		const setLastWrite = this.setLastWrite;
		const setLastSpeak = this.setLastSpeak;
		return props.languages.map(function(language) {
			let cName;
			let skillRatingsWrite = [
				<div></div>,
				<div></div>,
				<div></div>
			];
			let skillRatingsSpeak = [
				<div></div>,
				<div></div>,
				<div></div>
			];
			for (let i = 0; i < skillRatingsWrite.length; i++) {
				if(i === language.lastWrite) {
					let classList = 'skillpoints ' + styles.color + ' last';
					skillRatingsWrite[i] = <div key={i} onClick={(e) => setLastWrite(language.id, e)} className={classList}><div className="skillFix"></div></div>
				} else {
					let classList = 'skillpoints ' + styles.color;
					skillRatingsWrite[i] = <div key={i} onClick={(e) => setLastWrite(language.id, e)} className={classList}><div className="skillFix"></div></div>
				}
			};
			for (let i = 0; i < skillRatingsSpeak.length; i++) {
				if(i === language.lastSpeak) {
					let classList = 'skillpoints ' + styles.color + ' last';
					skillRatingsSpeak[i] = <div key={i} onClick={(e) => setLastSpeak(language.id, e)} className={classList}><div className="skillFix"></div></div>
				} else {
					let classList = 'skillpoints ' + styles.color;
					skillRatingsSpeak[i] = <div key={i} onClick={(e) => setLastSpeak(language.id, e)} className={classList}><div className="skillFix"></div></div>
				}
			};
			if(page.items.languages.includes(language.id)) {
				return (
					<div className="col-sm-12 paddingFix noBreak" key={language.id} ref={"languages_" + language.id + "_" + page.page}>
						<div className="col-sm-6 paddingFix">
						   <ul className="languages">
						      <li className="language" onKeyDown={(e) => checkLanguage(e, language.id)}>
						        <TextInput styles={styles} text={language.language} placeholder="Taal" onSave={(text) => props.actions.editLanguage(language.id, text)} autoFocus></TextInput>
						      </li>
						   </ul>
						</div>
						<div className="col-sm-6 languageContainer">
						   <div className="col-sm-12 languageItems">
						      <div className="languageWritingSkillRatings languageSpeakingSkillRatings">
						         <div className="languageWritingSkill languageSpeakingSkill">
						            <div className={sliderStyle}>
						               {skillRatingsWrite}
						            </div>
						            <div className="skillpoints hiding"></div>
						            <div className={sliderStyle}>
						            	{skillRatingsSpeak}
						            </div>
						         </div>
						      </div>
						   </div>
						</div>					
					</div>
				)
			} else {
				return;
			}
		})
	}

	render() {
		const styles = this.props.styles;
		const actions = this.props.actions;
		let classList = "headline " + styles.font;
		return(
			<div>
				<h3 className={classList}>Talen</h3>
				<PopOverLanguages actions={actions} styles={styles} />
				<div className="dividers">
						<div className="divider fat"></div>
						<div className="divider thin"></div>
				</div>
				<div>
					<span className="languageWritingSkill languageSpeakingSkill smallText right">
						<span>Schrijven</span>
						<span>Spreken</span>
					</span>
					{this.renderLanguages(this.props)}
				</div>
			</div>
		)
	}
}