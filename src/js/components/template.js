import React from 'react';
import ReactDOM from 'react-dom';
import TextInput from '../components/TextInput.js';
import Piechart from '../components/Piechart.js';
import Work from '../components/Work.js';
import Education from '../components/Education.js';
import Certification from '../components/Certification.js';
import Languages from '../components/Languages.js';
import SkillParent from '../components/SkillParent.js';
import Summary from '../components/Summary.js';
import Profile from '../components/Profile.js';
import Topbar from '../components/Topbar.js';
import Save from '../components/Save.js';

class Template extends React.Component {
	constructor() {
		super();
		this.detectOverflow = this.detectOverflow.bind(this);
		this.detectUnderflow = this.detectUnderflow.bind(this);
		this.renderTemplateByPageItems = this.renderTemplateByPageItems.bind(this);
		this.getThisPagesRefs = this.getThisPagesRefs.bind(this);
		this.changePage = this.changePage.bind(this);
		this.state = {
			refs: [

			]
		}
	}
	// globale function die alle refs ophaalt en pusht naar een array 

	detectOverflow(pageId) {
		const getThisPagesRefs = this.getThisPagesRefs;
		const changePage = this.changePage;
		const detectUnderflow = this.detectUnderflow;
		var observeDOM = (function(){
			var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
				eventListenerSupported = window.addEventListener;

			return function(obj, callback){
				if( MutationObserver ){
					// define a new observer
					var obs = new MutationObserver(function(mutations, observer){
						if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
							callback();
					});
					// have the observer observe foo for changes in children
					obs.observe( obj, { childList:true, subtree:true });
				}
				else if( eventListenerSupported ){
					obj.addEventListener('DOMNodeInserted', callback, false);
					obj.addEventListener('DOMNodeRemoved', callback, false);
				}
			};
		})();

		// Observe a specific DOM element:

		observeDOM( document.body ,function(){
		    let x = document.getElementsByClassName("wrapper");
		    let y = document.getElementsByClassName("cv");

		    let overflow = false;
			let wrapper = x[pageId - 1];
			if(y[pageId - 1] == undefined) {
				//just ignore it
			} else {
				let cvHeight = y[pageId - 1].clientHeight - 100;
				let remaining = cvHeight - wrapper.clientHeight;
				detectUnderflow(pageId, remaining);
				if(wrapper.clientHeight > cvHeight) {
					getThisPagesRefs(pageId);
					changePage();
				} else {
					// no need to check anything
				}
			}
		});
	}

	detectUnderflow(pageId, remainingHeight) {
		let allRefsNextPage = [];
		for(let k in this.refs) {
			if(parseInt(k.split("_")[1]) == (pageId + 1)) {
				allRefsNextPage.push(this.refs[k]);
				if(allRefsNextPage[0].refs[Object.keys(allRefsNextPage[0].refs)[0]] == undefined) {
					let htmlToCheck = document.getElementById('pieRef');
					if(htmlToCheck.clientHeight < remainingHeight) {
						//get the keyValue for this item
						let ref = ("pieChart_1_" + (pageId + 1)) //hacky, but we cant place a ref on piechart :(
						//send the trigger to change the page, but this time downwards
						let data = ref.split("_");
						let category = data[0];
						let instanceId = parseInt(data[1]);
						let pageID = parseInt(data[2]);
						this.props.actions.changePage(category, instanceId, pageID, "DOWN");
						// this.props.actions.checkPageForDelete(pageId);
						//check if the page with pageID is empty and can be deleted
					} else {
						//do nothing
					}
				} else {
					let htmlToCheck = allRefsNextPage[0].refs[Object.keys(allRefsNextPage[0].refs)[0]]
					if(htmlToCheck.clientHeight < remainingHeight) {
						//get the keyValue for this item
						let getRefs = [];
						for(let l in this.refs[k].refs) {
							getRefs.push(l);
						}
						if(getRefs[0] == undefined) {
							//ignore
						} else {
							//send the trigger to change the pace down a level
							let ref = getRefs[0];
							let data = ref.split("_");
							let category = data[0];
							let instanceId = parseInt(data[1]);
							let pageID = parseInt(data[2]);
							this.props.actions.changePage(category, instanceId, pageID, "DOWN");
							
						}
					}
				}
			}
		}
		this.props.actions.checkPageForDelete(pageId);
	}

	changePage() {
		let ref = this.state.refs[this.state.refs.length - 1];
		let data = ref.split("_");
		let category = data[0];
		let instanceId = parseInt(data[1]);
		let pageId = parseInt(data[2]);
		this.props.actions.changePage(category, instanceId, pageId, "UP");
	}

	getThisPagesRefs(thisPage){
		let pageRefs = [];
		for(let k in this.refs) {
			if(parseInt(k.split("_")[1]) == thisPage) {
				if(k.split("_")[0] == 'pieChart') {
					pageRefs.push("pieChart_1_" + thisPage);
				}
				for(let l in this.refs[k].refs) {
					pageRefs.push(l);
				}
			}
		}
		this.state.refs = pageRefs;
	}

	renderTemplateByPageItems() {
		const pages = this.props.pages;
		const actions = this.props.actions;
		const workExperiences = this.props.workExperiences;
		const workResponsibilities = this.props.workResponsibilities;
		const pieChart  = this.props.pieChart;
		const educations = this.props.educations;
		const languages = this.props.languages;
		const certifications = this.props.certifications;
		const skills = this.props.skills;
		const refs = this.refs;
		const summary = this.props.summary;
		const profile = this.props.profile;
		const styles = this.props.styles;
		const detectOverflow = this.detectOverflow;
		return pages.map(function(page) {
			let toplayout = [];
			let layout = [];
			if(page.items.profile == undefined) {
				//ignore this
			} else {
				for (var i = 0; i < page.items.profile.length; i++) {
					if(page.items.profile[i] == profile[i].id) {
						toplayout.push(
							<div className="col-md-4 profilePicture">
								<img src="http://lorempixel.com/400/400/cats" />
							</div>
						);
						toplayout.push(<Profile ref={"profile_" + page.page} page={page} actions={actions} styles={styles} profile={profile} />);
					}
				};
			}
			if(page.items.summary == undefined) {
				//ignore this
			} else {
				for(var i = 0; i < page.items.summary.length; i++){
					if(page.items.summary[i] == summary[i].id) {
						toplayout.push(<Summary ref={"summary_" + page.page} page={page} actions={actions} styles={styles} summary={summary} />);
					}
				};
			}
			if(page.items.workExperience == undefined) {
				//ignore this
			} else {
				if(page.items.workExperience.length == 0) {

				} else {
					layout.push(<Work ref={"workExperience_" + page.page} page={page} actions={actions} styles={styles} workExperiences={workExperiences} workResponsibilities={workResponsibilities} />);
				}
			}
			if(page.items.educations == undefined) {
				//ignore this
			} else {
				if (page.items.educations.length == 0) {
					//remove headline
				} else {
					layout.push(<Education ref={"education_" + page.page} page={page} actions={actions} styles={styles} educations={educations}/>);
				}
			}
			if(page.items.skills == undefined) {
				//ignore this
			} else {
				if(page.items.skills.length == 0) {
					//remove headline
				} else {
					layout.push(<SkillParent ref={"skills_" + page.page} page={page} actions={actions} styles={styles} skills={skills} />);
				}
			}
			if(page.items.languages == undefined) {
				//ignore this
			} else {
				if(page.items.languages.length == 0) {
					// remove headline
				} else {
					layout.push(<Languages ref={"languages_" + page.page} page={page} actions={actions} styles={styles} languages={languages} />);
				}
			}
			if(page.items.certifications == undefined) {
				//ignore this
			} else {
				if(page.items.certifications.length == 0) {
					//remove headline
				} else {
					layout.push(<Certification ref={"certifications_" + page.page} page={page} actions={actions} styles={styles} certifications={certifications} />);
				}
			}
			if(page.items.pieChart == undefined) {
				//ignore this
			} else {
				if(page.items.pieChart.length == 0) {
					//rempove headline
				} else {
					if(page.items.pieChart.includes(1)) {
						layout.push(<Piechart ref={"pieChart_" + page.page} page={page} actions={actions} styles={styles} pieChart={pieChart} />);
					} else {
						//no!
					}
				}
			}
			return(
				<div className="cv col-xs-12">
						<div className="col-md-12">
						<Save 
							profile={profile} 
							summary={summary}
							workExperiences={workExperiences} 
							workResponsibilities={workResponsibilities} 
							educations={educations} 
							certifications={certifications} 
							skills={skills} 
							languages={languages} 
							styles={styles}
						/>
					</div>
					<Topbar styles={styles}/>
					<div className='wrapper' id="wrapper">	
						
						{toplayout}
						<div className="innerWrapper">
							{layout}
						</div>
						
					
						
						{/*<Save 
								profile={profile} 
								summary={summary}
								workExperiences={workExperiences} 
								workResponsibilities={workResponsibilities} 
								educations={educations} 
								certifications={certifications} 
								skills={skills} 
								languages={languages} 
								styles={styles}
								pages={pages}
							/>*/}
						{detectOverflow(page.page)}
					</div>
				</div>
			);
		});
	}

	render(){
		return(
			<div id="templates" key='1'>
				{this.renderTemplateByPageItems()}
			</div>
		)
	}
}

export default Template;