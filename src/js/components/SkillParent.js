import React, { Component, PropTypes } from 'react';
import TextInput from './TextInput.js';
import PopOver from './Popover.js';

export default class SkillParent extends Component {
	constructor() {
		super();
		this.setLastWrite = this.setLastWrite.bind(this);
		this.checkSkill = this.checkSkill.bind(this);
		this.state = {
			skillId: 0,
			objectCount: 0
		}
	}
	componentWillMount(){
		let init;
		let check = this.props.skills[this.props.skills.length - 1];
		let newObjectCount = this.props.skills.length;
		if (check == undefined) {
			init = 0;
		} else {
			init = check.id
		}
		
		this.state.skillId = init;
		this.state.objectCount = newObjectCount;
		if(this.state.objectCount == 0){
			this.createSkill();
		}
	}

	componentDidMount(){
		function getNextSiblings(el) {
				let siblings = [];
				while (el= el.nextSibling) { siblings.push(el); }
				return siblings;
		}
		let last = document.querySelectorAll('.last');
		for (let i = 0; i < last.length; i++) {
			let getNextSibs = getNextSiblings(last[i]);
			for (let j = 0; j < getNextSibs.length; j++) {
				getNextSibs[j].style.opacity = "0.2"
			};
		};
	}

	setOpacity(el) {
		function getNextSiblings(el) {
				let siblings = [];
				while (el= el.nextSibling) { siblings.push(el); }
				return siblings;
		}
		function getPreviousSiblings(el) {
			let siblings = [];
			while (el = el.previousSibling) { siblings.push(el); }
			return siblings;
		}
		let nextSibs = getNextSiblings(el);
		let prevSibs = getPreviousSiblings(el);
		for (var i = 0; i < nextSibs.length; i++) {
			nextSibs[i].style.opacity = "0.2";
		}; 
		for (var i = 0; i < prevSibs.length; i++) {
			prevSibs[i].style.opacity = "1";
		};
		el.style.opacity = "1";
	}

	checkSkill(e, skillID) {
		if(e.keyCode == 8){
				if(e.target.value == ""){
					this.deleteSkill(skillID)
				}
		} else if(e.keyCode == 13) {
			this.createSkill();
		}
		if(this.state.objectCount == 0) {
			this.createSkill();
		}
	}

	createSkill() {
		this.state.objectCount++;
		this.state.skillId++;
		let id = this.props.skills[this.props.skills.length - 1].id;
		id =id + 1;
		this.props.actions.createSkill(id);
		this.props.actions.placePage(id, "skills", this.props.page.page)
	}

	deleteSkill(id) {
		this.state.objectCount--;
		this.props.actions.deleteSkill(id);
		this.props.actions.removePage(id, "skills", this.props.page.page)
	}

	setLastWrite(id, e) {
		let list = e.target.parentNode.children;
		let target = Array.prototype.indexOf.call(list, e.target);
		this.props.actions.editSkillWriting(id, target);
		this.setOpacity(e.target);
	}

	renderSkills(props){
		const page = this.props.page.page;
		const items = this.props.page.items;
		const styles = this.props.styles;
		const checkSkill = this.checkSkill;
		const setLastWrite = this.setLastWrite;
		let sliderStyle = 'skillRating ' + styles.sliderStyleSkills;
		return props.skills.map(function(skill) {
			let cName;
			let skillRatingsWrite = [
				<div></div>,
				<div></div>,
				<div></div>,
				<div></div>,
				<div></div>,
				<div></div>,
				<div></div>
			];
			for (var i = 0; i < skillRatingsWrite.length; i++) {
				if(i === skill.lastSkill) {
					let classList = 'skillpoints ' + styles.color + ' last';
					skillRatingsWrite[i] = <div key={i} onClick={(e) => setLastWrite(skill.id, e)} className={classList}></div>
				} else {
					let classList = 'skillpoints ' + styles.color;
					skillRatingsWrite[i] = <div key={i} onClick={(e) => setLastWrite(skill.id, e)} className={classList}></div>
				}
			};
			if(items.skills.includes(skill.id)) {
				return (
					<div className="col-sm-12 paddingFix" key={skill.id} ref={"skills_" + skill.id + "_" + page}>
						<div className="col-sm-4 paddingFix" onKeyDown={(e) => checkSkill(e, skill.id)}>
							<TextInput styles={styles} placeholder="Vaardigheid" text={skill.skill} onSave={(text) => props.actions.editSkill(skill.id, text)} autoFocus></TextInput>
						</div>
						<div className="col-sm-8 skillRatingsWrite">
						<li className={sliderStyle}>
							{skillRatingsWrite}
						</li>
						</div><br/>
					</div>
				)
			} else {
				return;
			}
		})
	}

	render() {
		const actions = this.props.actions;
		const styles = this.props.styles;
		let classList = 'headline ' + styles.font;
		return(
			<div class="col-md-12 noBreak">
				<h3 className={classList}>Vaardigheden</h3>
				<PopOver actions={actions} styles={styles}/>
				<div className="dividers">
						<div className="divider fat"></div>
						<div className="divider thin"></div>
				</div>
				<div>
					<span class="skillRating smallText right">
						<span>gemiddeld</span>
						<span>goed</span>
						<span>erg goed</span>
					</span>
					{this.renderSkills(this.props)}
				</div>
			</div>
		)
	}
}