import React from 'react';
import DownloadPDF from './downloadPDF.js';
import ColorPicker from './ColorPicker.js';
import FontSelector from './FontSelector.js';
import NewBlock from './NewBlock.js';
 
class Sidebar extends React.Component {

	constructor() {
		super();
		this.showSidebar = this.showSidebar.bind(this);
		this.closeSidebar = this.closeSidebar.bind(this);
		this.selectedComponent = this.selectedComponent.bind(this);
		this.state = {
			hidden: true,
			selectedColor: '',
			Menu: '',
			class: '',
			component: ''
		}
	}

	showSidebar(e) {
		let selectedMenu = e.target.id;
		this.setState({Menu: selectedMenu})
		this.setState({hidden : false});
	}

	closeSidebar() {
		this.selectedComponent = '';
		this.setState({hidden : true});
	}

	selectedComponent() {
		return;
	}

	render() {

		if (this.state.hidden) {
		  this.state.class = 'sidenav';
		} else {
		  this.state.class = 'sidenav open';
		}

		let element;
		if (this.state.Menu == "fontSelector") {
			element = ( <FontSelector actions={this.props.actions}/> )
		} else if (this.state.Menu == "colorPicker") {
 			element = ( <ColorPicker actions={this.props.actions}/>)
		} else if (this.state.Menu == "newBlock") {
			element = ( 
				<NewBlock actions={this.props.actions}
				pages={this.props.pages}
				workExperiences={this.props.workExperiences} 
				workResponsibilities={this.props.workResponsibilities} 
				pieChart={this.props.pieChart} 
				educations={this.props.educations} 
				languages={this.props.languages} 
				certifications={this.props.certifications} 
				skills={this.props.skills} 
				summary={this.props.summary} 
				profile={this.props.profile} 
				styles={this.props.styles}
			/>)
		}

		return(
			<div className="col-sm-12">
				<div className="col-sm-12 sidebarHeader">
					<div className="sidebarLogo">
						<img className="logo" src="../assets/images/uniekLogo.png"/>
						<DownloadPDF />
					</div>
				</div>
				<div className="col-sm-12 sidebarContent">
					<div className="options">
						<div className="fa fa-plus-circle option"><span id="newBlock" onClick={this.showSidebar}>Nieuw blok</span></div><br />
						<div className="fa fa-file-o option"><span onClick={this.showSidebar}>Nieuwe template</span></div><br />
						<div className="fa fa-font option"><span id="fontSelector" onClick={this.showSidebar}>Nieuw font</span></div><br />
						<div className="fa fa-pencil-square-o option"><span id="colorPicker" onClick={this.showSidebar}>Nieuwe kleuren</span></div><br />
					</div>
				</div>
				<div id="mySidenav" className={this.state.class}>
				<a href="javascript:void(0)" onClick={this.closeSidebar} className="closebtn" id="closebtn">&times;</a>
					{element}
				</div>
			</div>
		);
	}
}
export default Sidebar;