import React, { Component, PropTypes } from 'react';
import TextInput from './TextInput.js';
import TextArea from './TextArea.js';

export default class Certification extends Component {
	constructor() {
		super();
		this.addNav = this.addNav.bind(this);
		this.renderCertifications = this.renderCertifications.bind(this);
		this.addCertification = this.addCertification.bind(this);
		this.deleteCertification = this.deleteCertification.bind(this);
		this.state = {
			certificationId: 0,
			objectCount: 0
		}
	}

	componentWillMount(){
		let init;
		let check = this.props.certifications[this.props.certifications.length - 1];
		if (check == undefined) {
			init = 0;
			this.state.certificationId = init;
			this.addCertification();
		} else {
			init = check.id
			this.state.objectCount = this.props.certifications.length;
			this.state.certificationId = init;
		}
	}

	addCertification() {
		this.state.certificationId++;
		this.state.objectCount++;
		if(this.props.certifications[this.props.certifications.length - 1] == undefined) {
			let id = 1;
			this.props.actions.addCertification(id);
			this.props.actions.placePage(id, "certifications", this.props.page.page);
		} else {
			let id = this.props.certifications[this.props.certifications.length - 1].id;
			id = id + 1;
			this.props.actions.addCertification(id);
			this.props.actions.placePage(id, "certifications", this.props.page.page);
		}
	}

	deleteCertification(id) {
		this.state.objectCount--;
		this.props.actions.deleteCertification(id);
		if(this.state.objectCount <= 0){
			this.addCertification();
		}
		this.props.actions.removePage(id, "certifications", this.props.page.page);
	}

	addNav(e) {
		var d = e.target.parentNode;
		d.classList.toggle('nav-is-visible');
	}

	renderCertifications(data){
		const page = this.props.page;
		const styles = this.props.styles;
		return this.props.certifications.map(function(certification){
			if(page.items.certifications.includes(certification.id)) {
				return(
					<div className="certifications" key={certification.id} ref={"certifications_" + certification.id + "_" + page.page}>
						<TextInput styles={styles} text={certification.name} placeholder="Naam van de Cursus"onSave={(text) => data.props.actions.changeCertificationName(certification.id, text)}></TextInput>
						
						<br/>
						<TextArea styles={styles} text={certification.instituut} placeholder="Instituut" onSave={(text) => data.props.actions.changeCertificationInformation(certification.id, text)}></TextArea><br/><br/>
						<nav class="cd-stretchy-nav edit-content">
							<a class="cd-nav-trigger" href="#0" onClick={(e) => data.addNav(e)}>
								<span aria-hidden="true"></span>
							</a>

							<ul>
								<li><a href="#0" onClick={() => data.addCertification("")}></a></li>
								<li><a href="#0" onClick={() => data.deleteCertification(certification.id)}></a></li>
							</ul>

							<span aria-hidden="true" class="stretchy-nav-bg"></span>
						</nav>
					</div>
				)
			} else {
				return;
			}
		})
	}

	render() {
		const styles = this.props.styles;
		let classList = 'headline heading-font ' + styles.font;
		return (
			<div>
				<h3 className={classList}> Certificaten</h3>
				<div className="dividers">
					<div className="divider fat"></div>
					<div className="divider thin"></div>
				</div>
				<div>
					{this.renderCertifications(this)}
				</div>
			</div>
		)
	}

}