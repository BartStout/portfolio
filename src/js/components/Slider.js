import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class Slider extends Component {
	static PropTypes = {
		val:  PropTypes.string,
		text: PropTypes.string,
	}

	state = {
		val: this.props.val || '50',
		text: this.props.text || '',
		redraw: false
	}

	handleBlur = e => {
		this.setState({ redraw: true })
	}

	updateSliderInput(e) {
		this.setState({
			val: e.target.value
		});
		this.props.actions.editSliderValue(this.props.target, e.target.value);
	} 

	updateTextInput(e) {
		this.props.actions.editSliderLabel(this.props.target, e.target.value);
	}

	createDeleteSlider(el, e) {
		if(e.keyCode == 8){
			if(e.target.value == ""){
				let objectCount = document.querySelectorAll('.Slider');
				el.props.actions.deleteSlider(el.props.target);
				objectCount = objectCount.length - 1;
				if(objectCount == 0) {
					this.props.actions.addPie(1);
				}
			}
		} else if(e.keyCode == 13) {
			let id = this.props.actions.pieChart.data.sliceId[this.props.actions.pieChart.data.sliceId.length - 1];
			id = id + 1;
			el.props.actions.addPie(id);
		}
	}

	render() {
		const styles = this.props.styles;
		let classList = 'inp ' + styles.font; 
		return (
			<div className="Slider col-sm-12">
				<div class="text col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<input type="text" placeholder="text" onKeyDown={(e) => this.createDeleteSlider(this, e)} onChange={(e) => this.updateTextInput(e)} autoFocus />
				</div>
				<div class="range col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<input type="range" min="1" max="100" step="1" onInput={(e) => this.updateSliderInput(e)}/>
				</div>
			</div>
		);
	}
}

export default Slider;