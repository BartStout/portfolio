import React, {Component} from 'react';
import Slider from './Slider.js';

export default class PieOptions extends Component {
	constructor() {
		super();
		this.togglePieOptions = this.togglePieOptions.bind(this);
		this.stopEvent = this.stopEvent.bind(this);
		this.renderSliders = this.renderSliders.bind(this);
	}

	togglePieOptions(e) {
		e.target.childNodes[0].classList.toggle("display-pie");
	}
	stopEvent(e) {
		e.stopPropagation();
	}

	renderSliders(props) {
		const styles = this.props.styles;
		let toMap = props.data;
		return toMap.pieChart.data.sliceId.map(function(id, index){
			return <Slider styles={styles} actions={toMap} key={ id } target={id}/>
		});
	}

	render(){
		return(
			<div className="rightCog fa fa-cog fa-2x hiddenPieOptions" onClick={(e) => this.togglePieOptions(e)}>
				<div className="pieOptions" onClick={(e) => this.stopEvent(e)}>
					{this.renderSliders(this.props)}
				</div>
			</div>
		)
	}
}