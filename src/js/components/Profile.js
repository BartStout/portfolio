import React, { Component, PropTypes } from 'react';
import TextArea from './TextArea.js';

export default class Profile extends Component {
	constructor() {
		super();
		this.state = {
			objectCount: 0
		}
	}

	componentWillMount(){
		let check = this.props.profile.length;
		if(check == 0){
			this.state.objectCount++;
			this.props.actions.addProfile();
		} else {
			this.state.objectCount++;
		}
	}


	renderProfile(props){
		const action = props.actions 
		const styles = this.props.styles;
		let classList = 'round ' + this.props.styles.color;
		return props.profile.map(function(profile){
			return(
				<div className="col-sm-8 profileText" key={profile.id}>
				<h1 className="title"><TextArea styles={styles} placeholder="Naam Achternaam" text={profile.title} onSave={(text) => action.editProfileTitle(1, text)}></TextArea></h1>
				<h2 className="subtitle"><TextArea styles={styles} placeholder="Jezelf in twee woorden" text={profile.subtitle} onSave={(text) => action.editProfileSubTitle(1, text)}></TextArea></h2>
					
					<div className="col-sm-6">
						<div className="row margin-bottom">
							<div class="col-sm-1 padding-right">
								<div className={classList}>
									<span className="glyphicon glyphicon-user"></span>
								</div>
							</div>
							<div class="col-sm-10 padding-null">
								<span><TextArea styles={styles} placeholder="01 Januari 2016" text={profile.birth} onSave={(text) => action.editProfileBirth(1, text)}></TextArea></span>
							</div>    
						</div>
						<div className="row margin-bottom">
							<div class="col-sm-1 padding-right">
								<div className={classList}>
									<span className="glyphicon glyphicon-phone"></span>
								</div>
							</div>
							<div class="col-sm-10 padding-null">
								<span><TextArea styles={styles} placeholder="06 12 34 56 78" text={profile.phone} onSave={(text) => action.editProfilePhone(1, text)}></TextArea></span>
							</div>    
						</div>	
						<div className="row margin-bottom">
							<div class="col-sm-1 padding-right">
								<div className={classList}>
									<span className="glyphicon glyphicon-envelope"></span>
								</div>
							</div>
							<div class="col-sm-10 padding-null">
								<span><TextArea styles={styles} placeholder="mail@email.com" text={profile.email} onSave={(text) => action.editProfileEmail(1, text)}></TextArea></span>
							</div>    
						</div>		
					</div>
					<div className="col-sm-6">
						<div className="row margin-bottom">
							<div class="col-sm-1 padding-right">
								<div className={classList}>
									<span className="fa fa-map-marker"></span>
								</div>
							</div>
							<div class="col-sm-10 padding-null">
								<span><TextArea styles={styles} placeholder="Plaats hier uw adres" text={profile.location} onSave={(text) => action.editProfileLocation(1, text)}></TextArea></span>
							</div>    
						</div>

						<div className="row margin-bottom">
							<div class="col-sm-1 padding-right">
								<div className={classList}>
									<span className="fa fa-linkedin"></span>
								</div>
							</div>
							<div class="col-sm-10 padding-null">
								<span><TextArea styles={styles} placeholder="01 Januari 2016" text={profile.birth} onSave={(text) => action.editProfileBirth(1, text)}></TextArea></span>
							</div>    
						</div>
					</div>	
			</div>
			)
		})
	}

	render(){
		return(
			<div>
				{this.renderProfile(this.props)}
			</div>
		)
	}
}