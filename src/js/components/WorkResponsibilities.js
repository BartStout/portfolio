import React, { Component, PropTypes } from 'react';
import TextInput from './TextInput.js';
import TextArea from './TextArea.js';

export default class WorkResponsibility extends React.Component {
  render(){
    return (
      <div>
          {this.renderWorkResponsibilities()}
      </div>
    )
  }

  checkResp(e, props) {
    if(e.keyCode == 8){
      let parent = props.propData.workExperiences.findIndex(function(work){
        return work.id === props.parentExperience
      });
      let objectCount = props.propData.workExperiences[parent].responsibilities.length;
      if(e.target.value == "") {

        objectCount = objectCount - 1;
        props.propData.actions.deleteWorkResponsibility(props.data.id, props.parentExperience);
        if(objectCount == 0) {
          let check = props.propData.workResponsibilities[props.propData.workResponsibilities.length - 1].id;
          if (check == undefined) {
            let id = 1;
            props.propData.actions.addWorkResponsibility(id, props.parentExperience);
          } else {
            let id = check + 1;
            props.propData.actions.addWorkResponsibility(id, props.parentExperience);
          }
          
        }
      } else {
        //do nothing
      }
    } else if(e.keyCode == 13) {
      e.preventDefault();
      let id = props.propData.workResponsibilities[props.propData.workResponsibilities.length - 1].id;
      id = id + 1;
      props.propData.actions.addWorkResponsibility(id, props.parentExperience);
    }
    
  }

  renderWorkResponsibilities() {
    const styles = this.props.styles;
    const checkResp = this.checkResp;
      return(
         <div className="workR" key={this.props.data.id} onKeyDown={(e) => this.checkResp(e, this.props)}>
           <li><TextArea styles={styles} text={this.props.data.task} placeholder="Vul hier taken, verantwoordelijkheden en resultaten in" onSave={(text) => this.props.propData.actions.editWorkResponsibility(this.props.data.id, text)}></TextArea></li>  
        </div>
      );
  }
}