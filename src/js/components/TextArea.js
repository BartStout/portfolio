import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class TextArea extends React.Component {
  static PropTypes = {
    onSave: PropTypes.func.isRequired,
    text: PropTypes.string,
    placeholder: PropTypes.string,
    editing: PropTypes.bool,
  }
  
  state = {
    text: this.props.text || ''
  }

  handleChange = e => {
    this.setState({ text: e.target.value })
  }

  handleBlur = e => {
    this.props.onSave(e.target.value)
  }

  handleResize = e => {
    e.target.style.height = '1px';
    e.target.style.height = (e.target.scrollHeight) + 'px';
  }

  componentDidMount(){
    let init = ReactDOM.findDOMNode(this);
    init.style.height = '1px';
    init.style.height = (init.scrollHeight) + 'px';
  }

  render(){
    const styles = this.props.styles;
    let classList = 'TextArea ' + styles.font;
    return(
      <textarea className={classList}
        type="text"
        placeholder={this.props.placeholder}
        value={this.state.text}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleResize}
        onKeyPress={this.handleResize}
      />
    )
  }
}