import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class NewBlock extends React.Component {
	constructor() {
		super();
		this.addBlock = this.addBlock.bind(this);
	}

	addBlock(type) {
		let pageId = this.props.pages[this.props.pages.length - 1].page;
		if(type == "workExperience") {
			console.log(this.props.workExperiences)
			let id = this.props.workExperiences[this.props.workExperiences.length - 1].id;
			console.log(id)

			if(id == undefined) {
				id = 0;
			}
			id = id + 1;
			this.props.actions.addWorkExperience("", id);
			this.props.actions.placePage(id, type, pageId);

		} else if (type == "educations") {

				let id = this.props.educations[this.props.educations.length - 1].id;
				console.log(this.props.educations[0].id);


				console.log(this.props.educations);
				if(id == undefined) {
					id = 0;
				} else {
					id += 1;
				}
				console.log(id)
				this.props.actions.addEducation("", id);
				// bug check place in page action

				this.props.actions.placePage(id, type, pageId);
		} else if (type == "Certification") {
				let id = this.props.certifications[this.props.certifications.length - 1].id;
				if(id == undefined) {
					id = 0;
				}
				id = id + 1;
				this.props.actions.addCertification("", id);
				this.props.actions.placePage(id, type, pageId);
		} else if (type == "Skills") {
				let id = this.props.skills[this.props.skills.length - 1].id;

				if(id == undefined) {
					id = 0;
				}
				id = id + 1;
				this.props.actions.addEducation("", id)
				this.props.actions.placePage(id, type, pageId);
		} else if (type == "pieChart") {
				let id = this.props.workExperiences[this.props.workExperiences.length - 1].id;
				if(id == undefined) {
					id = 0;
				}
				id = id + 1;
				this.props.actions.addEducation("", id)
				this.props.actions.placePage(id, type, pageId);
		} else {
			//ignore it
		}
	}

  render(){
	return(
		<div>
			<h3 class="navtitle">Nieuw Blok</h3>
			<div className="row">
				<div className="col-sm-12">
					<a className="newComponentItem" onClick={() => this.addBlock("workExperience")}>Werkervaring</a>
					<a className="newComponentItem" onClick={() => this.addBlock("educations")}>Opleidingen</a>
					<a className="newComponentItem" onClick={() => this.addBlock("Certification")}> Certificaten</a>
					<a className="newComponentItem" onClick={() => this.addBlock("Skills")}>Vaardigheden</a>
					<a className="newComponentItem" onClick={() => this.addBlock("pieChart")}>Interesses</a>
					<a className="newComponentItem" onClick={() => this.addBlock("Languages")}>Talen</a>
				</div>
			</div>
		</div>
	)
  }
}