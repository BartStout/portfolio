import React, { Component, PropTypes } from 'react';
import TextInput from './TextInput.js';
import TextArea from './TextArea.js';

export default class Education extends Component {
	constructor() {
		super();
		this.addNav = this.addNav.bind(this);
		this.renderEducations = this.renderEducations.bind(this);
		this.addEducation = this.addEducation.bind(this);
		this.deleteEducation = this.deleteEducation.bind(this);
		this.state = {
			educationId: 0,
			objectCount: 0
		}
	}

	componentWillMount(){
		let init;
		let check = this.props.educations[this.props.educations.length - 1];
		if (check == undefined) {
			init = 0;
			this.state.educationId = init;
			this.addEducation("");
		} else {
			init = check.id
			this.state.objectCount = this.props.educations.length;
			this.state.educationId = init;
		}
		//insert this components id into pages.
	}

	addEducation(name) {
		this.state.educationId++;
		this.state.objectCount++;
		let id = this.props.educations[this.props.educations.length - 1].id;
		id = id + 1;
		this.props.actions.addEducation(id, name);
		this.props.actions.placePage(id, "educations", this.props.page.page);
	}

	deleteEducation(id) {
		this.state.objectCount--;
		this.props.actions.deleteEducation(id);
		if(this.state.objectCount <= 0){
			this.addEducation("");
		}
		this.props.actions.removePage(id, "educations", this.props.page.page);
	}

	addNav(e) {
		var d = e.target.parentNode;
		d.classList.toggle('nav-is-visible');
	}

	renderEducations(data){
		const items = this.props.page.items.educations;
		const page = this.props.page.page;
		const styles = this.props.styles;
		return this.props.educations.map(function(education){
			if(items.includes(education.id)) {
				return(
					<div ref={'educations_' + education.id + '_' + page} className="educationItem" key={education.id}>
						<span className="customBullet"><div className="fa fa-circle-o" aria-hidden="true"/></span>
						<h4><TextInput styles={styles} placeholder="Naam Opleiding"text={education.name} onSave={(text) => data.props.actions.changeEducationName(education.id, text)}></TextInput>
						</h4>
						<TextInput styles={styles} placeholder="Plaats"text={education.location} onSave={(text) => data.props.actions.changeEducationLocation(education.id, text)}></TextInput><br/>
						<ul>
							<li>
								<TextArea styles={styles} placeholder="Vul hier aanvullende informatie in over de opleiding" text={education.info} onSave={(text) => data.props.actions.changeEducationInformation(education.id, text)}></TextArea>
							</li>
						</ul>
						<br/>
						<nav class="cd-stretchy-nav edit-content">
							<a class="cd-nav-trigger" href="#0" onClick={(e) => data.addNav(e)}>
								<span aria-hidden="true"></span>
							</a>

							<ul>
								<li><a href="#0" onClick={() => data.addEducation("")}></a></li>
								<li><a href="#0" onClick={() => data.deleteEducation(education.id)}></a></li>
							</ul>

							<span aria-hidden="true" class="stretchy-nav-bg"></span>
						</nav>
					</div>
				)
			} else {
				//do no create this item
			}
		})
	}

	render() {
		const data = this.props;
		const styles = this.props.styles;
		let classList = 'headline heading-font educationBlock ' + styles.font;
		return (
			<div>
				<h3 className={classList} > Opleidingen</h3>
				<div className="bordercontainer">
						<div className="dividers">
								<div className="divider fat"></div>
								<div className="divider thin"></div>
						</div>
				</div>
				<div className="educationObjects">
				{this.renderEducations(this)}
				</div>
			</div>
		)
	}

}