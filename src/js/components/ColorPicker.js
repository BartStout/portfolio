import React from 'react';

class ColorPicker extends React.Component {
	static propTypes = {
		color: React.PropTypes.string
	}

	constructor() {
		super();
		this.selectColor = this.selectColor.bind(this);
		this.state = {
			hidden: true,
			selectedColor: '',
			class: '',
		}
	}

	selectColor(e){
		let currentColor = e.target.id;
		this.setState({selectedColor: currentColor });
		this.props.actions.editColor(currentColor);
	}

	render() {
		return(
			<div>
				<h3 class="navtitle">Kies een accent kleur</h3>
				<div className="row">
			  	    <div className="color-box blue col-sm-12" id="blue" onClick={this.selectColor}>
			  	    	<div id="blue" onClick={this.selectColor}>
			  	    		<span className="colorName" id="blue" onClick={this.selectColor}>Emerald Blue</span>
			  	    		<span className="colorDescription" id="blue" onClick={this.selectColor}>rustgevend, kalm en zelfverzekerd</span>
			  	    	</div>
			  	    </div>
			  	</div>
			  	<div className="row">
			  	    <div className="color-box orange col-sm-12" id="orange" onClick={this.selectColor}>
			  	    	<div id="orange" onClick={this.selectColor}>
			  	    		<span className="colorName" id="orange" onClick={this.selectColor}>Deep Orange</span>
			  	    		<span className="colorDescription" id="orange" onClick={this.selectColor}>warm, intiem ene zorgzaam</span>
			  	    	</div>
			  	    </div>
			  	</div>
				<div className="row">
			  	    <div className="color-box red col-sm-12" id="red" onClick={this.selectColor}>
				  	    <div id="red" onClick={this.selectColor}>
			  	    		<span className="colorName" id="red" onClick={this.selectColor}>Red</span>
			  	    		<span className="colorDescription" id="red" onClick={this.selectColor}>rustgevend, kalm en zelfverzekerd</span>
				  	   	</div>
				  	</div>
			  	</div>
				<div className="row">
				  	<div className="color-box green col-sm-12" id="green" onClick={this.selectColor}>
				  		<div id="green" onClick={this.selectColor}>
					  		<span className="colorName" id="green" onClick={this.selectColor}>Green</span>
					  		<span className="colorDescription" id="green" onClick={this.selectColor}>warm, intiem ene zorgzaam</span>
				  		</div>
				  	</div>
				</div>
			</div>
		)
	}
}

export default ColorPicker;