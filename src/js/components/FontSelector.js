import React from 'react';

class FontSelector extends React.Component {

	constructor() {
		super();
		this.changeFont = this.changeFont.bind(this);
	}

	changeFont(e){
		let current = e.target;
		let elements = document.getElementsByClassName("active-font");
		let fontType = e.target.id;

		elements[0].classList.remove("active-font");
		current.classList.add("active-font");


		this.props.actions.editFont(fontType);
	}
	
	render() {
		return(
			<div>
				<h3 class="navtitle">Kies een font</h3>
				<div className="fonts-container col-sm-10">
					<a style={{fontFamily: 'Lato'}}    id="Lato" class="active-font" onClick={this.changeFont}>Lato</a>
					<a style={{fontFamily: 'Arial'}}   id="Arial" onClick={this.changeFont}>Arial</a>
					<a style={{fontFamily: 'Verdana'}} id="Verdana" onClick={this.changeFont}>Verdana</a>
					<a style={{fontFamily: 'Cambria'}} id="Cambria" onClick={this.changeFont}>Cambria</a>
					<a style={{fontFamily: 'Raleway'}} id="Raleway" onClick={this.changeFont}>Raleway</a>
					<a style={{fontFamily: 'Montserrat'}} id="Montserrat" onClick={this.changeFont}>Montserrat</a>
					<a style={{fontFamily: 'Roboto'}} id="Roboto" onClick={this.changeFont}>Roboto</a>
				</div>				
			</div>
		)
	}
}

export default FontSelector;