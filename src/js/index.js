import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './containers/App'
import reducers from './reducers'
import axios from 'axios';
	
var hydrate = confirm("wilt u een bestaande cv inladen?");
if(hydrate === true) {
	axios({
		method: 'post',
		url: 'http://127.0.0.1/ReduxClean/src/src/load.php'
	}).then(function(response){
		let Pdata = response.data;
		const store = createStore(reducers, Pdata, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
		function addPie(Pdata) {
			const hydrateP = {
				type: "LOAD_PIE",
				Pdata
			}
			store.dispatch(hydrateP);
		}
		addPie(Pdata.pieChart);
		function addPage() {
			const page = {
				type: "NEW_PAGE"
			}
			store.dispatch(page);
		}
		addPage();
		let target = Pdata.workExperiences.length;
		target = target - 1;
		let id = Pdata.workExperiences[target].id;
		render(
		  <Provider store={store}>
		    <App workExperienceId={id} />
		  </Provider>,
		  document.getElementById('root')
		)
		store.subscribe(() => {
		  console.log("store changed", store.getState());
		  // checkDocumentHeight();
		});
	})
} else {
	const storeFallBack = {"profile":[{"id":1,"birth":"birth","phone":"phone","email":"email","location":"location","linked":"linked"}],"summary":[{"id":1,"summary":"dit is een ingeladen text"}],"workExperiences":[{"id":1,"jobTitle":"developer","company":"BNG","place":"Rotterdam","responsibilities":[2]},{"id":2,"jobTitle":"developer","company":"TestLoad","place":"Rotterdam","responsibilities":[3]}],"workResponsibilities":[{"id":2,"work_id":1,"task":"i make websites pretty"}],"educations":[{"id":1,"name":"","location":"","info":""}],"certifications":[{"id":2,"name":"","instituut":""}],"skills":[{"id":1,"skill":"","lastSkill":5}],"languages":[{"id":1,"language":"testing swag","lastSpeak":0,"lastWrite":0}]};

	const store = createStore(reducers, storeFallBack, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
	function addPage() {
			const page = {
				type: "NEW_PAGE"
			}
			store.dispatch(page);
		}
		addPage();
	render(
	  <Provider store={store}>
	    <App/>
	  </Provider>,
	  document.getElementById('root')
	)
	
	// store.subscribe(() => {
	// 	console.log(store.getState())
	// });
}