-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Gegenereerd op: 16 dec 2016 om 11:55
-- Serverversie: 5.5.42
-- PHP-versie: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `uniekcv`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `json` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `store`
--

INSERT INTO `store` (`id`, `json`) VALUES
(24, '{"workExperiences":[{"id":1,"jobTitle":"developer","company":"BNG","place":"Rotterdam","responsibilities":[2]}],"workResponsibilities":[{"id":2,"work_id":1,"task":"i make websites pretty"}]}');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;